## The attached words.csv file contains two columns.
## The first column is a word and the second is an associated floating point number.
##
## Write a program which computes the average of the numbers and prints out this average.
##
## The program should also print the word or words closest to the average but not larger than the average.
##
## Your code should be written using c# 3.5 or above and run as a console application.
import csv

def main():
    sum = 0.0
    count = 0
    with open("words.csv") as words_file:
        for word, value in csv.reader(words_file):
            sum += float(value)
            count += 1
    average = None
    if count > 0:
        average = sum/count
        print "average: " + str(average)

    closest_value = None
    closest_words = []
    with open("words.csv") as words_file:
        for word, value_str in csv.reader(words_file):
            value = float(value_str)
            if value <= average:
                if closest_value == None or value > closest_value:
                    closest_value = value
                    closest_words = [word]
                elif value == closest_value:
                    closest_words.append(word)
    if closest_value is not None:
        print "closest value: " + str(closest_value)
        print "closests words: " + ', '.join(closest_words)







main()
